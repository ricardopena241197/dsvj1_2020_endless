#include "window.h"
#include "raylib.h"

namespace window
{
	static Screen SD;	
	static Screen qHD;
	static Screen HD;
	static Screen FHD;

	Screen screen;

	void init()
	{	
		//Initialize all qualities
		//SD
		SD.width = 640;
		SD.height = 480;
		SD.quality = ScreenQuality::SD;

		//qHD
		qHD.width = 960;
		qHD.height = 540;
		qHD.quality = ScreenQuality::qHD;

		//HD
		HD.width = 1280;
		HD.height = 720;
		HD.quality = ScreenQuality::HD;

		//FHD
		FHD.width = 1800;
		FHD.height = 980;
		FHD.quality = ScreenQuality::FHD;

		//Standard game screen
		screen = FHD;
		InitWindow(screen.width, screen.height, title);
		SetTargetFPS(60);
	}

	void changeResolution(ScreenQuality desiredQuality)
	{
		switch (desiredQuality)
		{
		case ScreenQuality::SD:
			screen = SD;
			break;
		case ScreenQuality::qHD:
			screen = qHD;
			break;
		case ScreenQuality::HD:
			screen = HD;
			break;
		case ScreenQuality::FHD:
			screen = FHD;
			break;
		default:
			break;
		}

		//Close the window and open it with the new size
		CloseWindow();
		InitWindow(screen.width,screen.height, title);
	}
}