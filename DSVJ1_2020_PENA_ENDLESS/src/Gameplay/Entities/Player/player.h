#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"

enum class Lanes
{
	Upper,
	Middle,
	Lower
};

enum class States
{
	Flying,
	Runing,
	MoovingUp,
	MoovingDown,
	JumpingUp,
	JumpingDown
};
struct Character
{
	Rectangle collider;
	Rectangle animationFrame;
	
	States characterState;

	int jumpingSpeed;
	int laneChangeSpeed;
	Lanes actualLane;

	int actualFrame;
	float frameTimer;

	Texture2D texture;
	Color dye;
};

struct Player
{
	int score;
	int distance;
	int lives;

	Character character;
};

namespace player
{
	const int maxFrames = 7;
	const float frameChangeTime = 0.05f;
}

#endif // !PLAYER_H

