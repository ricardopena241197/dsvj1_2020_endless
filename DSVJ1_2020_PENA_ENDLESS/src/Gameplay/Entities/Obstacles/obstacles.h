#ifndef OBSTACLES_H
#define OBSTACLES_H

#include "raylib.h"
#include "Gameplay/Entities/Player/player.h"

const int obstacleTipes = 3;
enum class ObstacleTipe
{
	Jumpeable,
	Breakable,
	Skippeable
};

struct Obstacle
{
	ObstacleTipe tipe;
	Rectangle collider;	
	Color dye;
	int speed;
	bool active;
	Lanes lane;
};

namespace obstacles
{

}

#endif // OBSTACLES_H

