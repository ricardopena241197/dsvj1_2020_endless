#include "gameplay.h"
#include "Game/game.h"
#include "Window/window.h"
#include "Gameplay/Entities/Player/player.h"
#include "Gameplay/Entities/Obstacles/obstacles.h"
#include "Gameplay/Stage/stage.h"
#include "Textures/textures.h"
#include "Controls/controls.h"
#include "Collisions/collisions.h"

#include <iostream>
#include <time.h>


namespace gameplay
{
	bool initializedMenu = false;

	//Entities
	Player player1;
	Stage level1;
	Obstacle obstacles[maxAmountObstacles];
	Texture2D obstaclesTextures[obstacleTipes];

	//HUDs 
		//Points
	Vector2 scorePos;
	int scoreSize;
	Color scoreColor;

		//Distance
	Vector2 distancePos;
	int distanceSize;
	Color distanceColor;

		//Lives
	Texture2D livesTexture;
	Vector2 livesPos;
	int livesSize;
	Color livesColor;

	//Timers
	clock_t obstaclesTimer;
	int timeIntervalForObstacles;

	//Auxiliars
	int auxTargetYPos;
	int auxJumpDistance;

	namespace initialize
	{
		static void initStage()
		{
			using namespace textures;

			//Init Background
			level1.background.texture = LoadTexture(backgroundTexturePath);
			level1.background.texture.height = window::screen.height / 2 * 1.25;
			level1.background.texture.width = window::screen.width;
			level1.background.dye = WHITE;

			level1.background.pos.x = 0;
			level1.background.pos.y = 0;
			level1.background.auxPosX = level1.background.pos.x + level1.background.texture.width;

			level1.background.speed = (window::screen.width / 30);

			//Init FrontBackground
			level1.background.frontBackground.texture = LoadTexture(frontBackgroundTexturePath);
			level1.background.frontBackground.texture.height = (window::screen.height / 2) * 1.25;
			level1.background.frontBackground.texture.width = window::screen.width;
			level1.background.frontBackground.dye = WHITE;

			level1.background.frontBackground.pos.x = 0;
			level1.background.frontBackground.pos.y = level1.background.pos.y + level1.background.texture.height / 4;
			level1.background.frontBackground.auxPosX = level1.background.frontBackground.pos.x + level1.background.frontBackground.texture.width - 2;

			level1.background.frontBackground.speed = window::screen.width / 20;

			//Init floor
			level1.floor.texture = LoadTexture(floorTexturePath);
			level1.floor.texture.height = (window::screen.height / 3);
			level1.floor.texture.width = window::screen.width;
			level1.floor.dye = WHITE;

			level1.floor.pos.x = 0;
			level1.floor.pos.y = (window::screen.height / 3) * 2;
			level1.floor.auxPosX = level1.floor.pos.x + level1.floor.texture.width - 1;

			level1.floor.speed = window::screen.width / 6;

			for (short i = 0; i < amountOfLanes; i++)
			{
				level1.lanePosY[i] = (int)(level1.floor.pos.y +(level1.floor.texture.height / 4) *( i + 1));
			}
		}	

		static void initPlayer()
		{
			using namespace textures;
			
			player1.distance = 0;
			player1.lives = 2;
			player1.score = 0;

			player1.character.collider.x = window::screen.width/14;
			player1.character.collider.y = level1.lanePosY[1];
			player1.character.collider.height = window::screen.height/16;
			player1.character.collider.width = player1.character.collider.height;

			player1.character.texture = LoadTexture(characterTexturePath);
			player1.character.texture.width = player1.character.collider.height* player::maxFrames;
			player1.character.texture.height = player1.character.collider.height;

			player1.character.animationFrame.x = 0;
			player1.character.animationFrame.y = 0;
			player1.character.animationFrame.height = player1.character.collider.height;
			player1.character.animationFrame.width = player1.character.collider.height;

			player1.character.actualFrame = 0;
			player1.character.frameTimer = 0.0f;

			player1.character.dye = WHITE;

			player1.character.laneChangeSpeed = window::screen.height / 5;
			player1.character.jumpingSpeed = (window::screen.height / 4) * -1;
			player1.character.actualLane = Lanes::Middle;
			player1.character.characterState = States::Runing;

		}

		static void initObstaclesTextures()
		{
			using namespace textures;

			obstaclesTextures[0] = LoadTexture(jumpeableObsTexturePath);

			obstaclesTextures[0].height = player1.character.collider.height * 2;
			obstaclesTextures[0].width = player1.character.collider.height;

			obstaclesTextures[1] = LoadTexture(breakableObsTexturePath);

			obstaclesTextures[1].height = player1.character.collider.height;
			obstaclesTextures[1].width = player1.character.collider.height;


			obstaclesTextures[2] = LoadTexture(skippeableObsTexturePath);

			obstaclesTextures[2].height = player1.character.collider.height * 2;
			obstaclesTextures[2].width = player1.character.collider.height * 2;
		}

		static void initObstacle(Obstacle & obstacle)
		{
			obstacle.dye = WHITE;
			obstacle.tipe = (ObstacleTipe)(rand() % obstacleTipes);
			obstacle.speed = level1.floor.speed;
			obstacle.active = false;

			switch (obstacle.tipe)
			{
			case ObstacleTipe::Jumpeable:

				obstacle.collider.height = player1.character.collider.height * 2;
				obstacle.collider.width = player1.character.collider.height;				

				break;
			case ObstacleTipe::Breakable:

				obstacle.collider.height = player1.character.collider.height ;
				obstacle.collider.width = player1.character.collider.height;

				break;
			case ObstacleTipe::Skippeable:

				obstacle.collider.height = player1.character.collider.height*2;
				obstacle.collider.width = player1.character.collider.height*2;

				break;
			default:
				break;
			}

			obstacle.lane = (Lanes)(rand() % amountOfLanes);

			switch (obstacle.lane)
			{
			case Lanes::Upper:
				obstacle.collider.y = level1.lanePosY[(int)obstacle.lane] - obstacle.collider.height + player1.character.collider.height;
				break;
			case Lanes::Middle:
				obstacle.collider.y = level1.lanePosY[(int)obstacle.lane] - obstacle.collider.height + player1.character.collider.height;
				break;
			case Lanes::Lower:
				obstacle.collider.y = level1.lanePosY[(int)obstacle.lane]- obstacle.collider.height+player1.character.collider.height;

				break;
			default:
				break;
			}

			obstacle.collider.x = window::screen.width + 5;
		}

		static void initHUDs()
		{
			using namespace textures;
			//Points
			scorePos.x = window::screen.width / 100;
			scorePos.y = window::screen.height / 100;
			scoreSize = window::screen.height / 30;
			scoreColor = WHITE;

			//Distance
			distancePos.x = (window::screen.width / 100)*30;
			distancePos.y = window::screen.height / 100;
			distanceSize = window::screen.height / 30;
			distanceColor = WHITE;

			//Lives
			livesTexture = LoadTexture(livesTexturePath);
			livesTexture.height = window::screen.height / 20;
			livesTexture.width = livesTexture.height;
			livesPos.x = (window::screen.width / 100) * 60;
			livesPos.y = window::screen.height / 100;
			livesSize = window::screen.height / 30;
			livesColor = WHITE;
		}		
	}

	namespace updating
	{
		static void runCharacterAnimation()
		{
			player1.character.frameTimer += GetFrameTime();

			if (player1.character.frameTimer >= player::frameChangeTime)
			{
				player1.character.frameTimer = 0.0;
				if (player1.character.actualFrame < player::maxFrames)
				{
					player1.character.actualFrame++;
				}
				else
				{
					player1.character.actualFrame = 0;
				}
				player1.character.animationFrame.x = player1.character.animationFrame.width * player1.character.actualFrame;
			}			
		}

		static void characterSideMovementSetting(Movement direction)
		{		
			if (player1.character.characterState == States::Runing)
			{
				if (direction == Movement::Up && player1.character.actualLane != Lanes::Upper)
				{
					player1.character.characterState = States::MoovingUp;
					player1.character.laneChangeSpeed = (player1.character.laneChangeSpeed > 0) ? player1.character.laneChangeSpeed * -1 : player1.character.laneChangeSpeed;

					switch (player1.character.actualLane)
					{
					case Lanes::Middle:
						auxTargetYPos = level1.lanePosY[0];
						player1.character.actualLane = Lanes::Upper;
						break;
					case Lanes::Lower:
						auxTargetYPos = level1.lanePosY[1];
						player1.character.actualLane = Lanes::Middle;
						break;

					default:
						break;
					}

				}
				else
				{
					if (direction == Movement::Down && player1.character.actualLane != Lanes::Lower)
					{
						player1.character.characterState = States::MoovingDown;
						player1.character.laneChangeSpeed = (player1.character.laneChangeSpeed < 0) ? player1.character.laneChangeSpeed * -1 : player1.character.laneChangeSpeed;

						switch (player1.character.actualLane)
						{
						case Lanes::Upper:
							auxTargetYPos = level1.lanePosY[1];
							player1.character.actualLane = Lanes::Middle;
							break;
						case Lanes::Middle:
							auxTargetYPos = level1.lanePosY[2];
							player1.character.actualLane = Lanes::Lower;
							break;

						default:
							break;
						}
					}
				}
			}
			
		}

		static void characterLaneSwaping()
		{			
			if (player1.character.characterState == States::MoovingUp)
			{
				if (player1.character.collider.y > auxTargetYPos)
				{
					player1.character.collider.y += player1.character.laneChangeSpeed * GetFrameTime();
				}
				else
				{
					player1.character.characterState = States::Runing;
				}
			}
			else
			{
				if (player1.character.characterState == States::MoovingDown)
				{
					if (player1.character.collider.y < auxTargetYPos)
					{
						player1.character.collider.y += player1.character.laneChangeSpeed * GetFrameTime();
					}
					else
					{
						player1.character.characterState = States::Runing;
					}
				}
			}
		}

		static void characterJumpSetting()
		{
			if (player1.character.characterState==States::Runing)
			{
				auxTargetYPos = player1.character.collider.y - auxJumpDistance;
				player1.character.characterState = States::JumpingUp;
				player1.character.jumpingSpeed = (player1.character.jumpingSpeed > 0) ? player1.character.jumpingSpeed * -1 : player1.character.jumpingSpeed;
			}
		}

		static void characterJumping()
		{
			if (player1.character.characterState == States::JumpingUp )
			{
				if (player1.character.collider.y > auxTargetYPos)
				{
					player1.character.collider.y += player1.character.jumpingSpeed * GetFrameTime();
				}
				else
				{
					player1.character.jumpingSpeed *= -1;	
					player1.character.characterState = States::JumpingDown;
				}				
			}
			else
			{
				if (player1.character.characterState == States::JumpingDown)
				{
					if (player1.character.collider.y < auxTargetYPos + auxJumpDistance)
					{
						player1.character.collider.y += player1.character.jumpingSpeed * GetFrameTime();
					}
					else
					{
						player1.character.characterState = States::Runing;
					}
				}
			}
		}

		static void movingObtacles()
		{
			for (short i = 0; i < maxAmountObstacles; i++)
			{
				if (obstacles[i].active)
				{
					if (obstacles[i].collider.x + obstacles[i].collider.width > 0)
					{
						obstacles[i].collider.x -= obstacles[i].speed * GetFrameTime();
					}
					else
					{
						obstacles[i].active = false;
					}					
				}
			}
		}

		static void generateObstacles()
		{
			clock_t auxTimer = clock();
			if (auxTimer - obstaclesTimer >= timeIntervalForObstacles)
			{
				obstaclesTimer = auxTimer;
				for (short i = 0; i < maxAmountObstacles; i++)
				{
					if (!obstacles[i].active)
					{
						initialize::initObstacle(obstacles[i]);
						obstacles[i].active = true;
						i = maxAmountObstacles;
					}					
				}
			}

		}

		static void imput()
		{
			using namespace controls;

			if (IsKeyPressed(keyUp))
			{
				characterSideMovementSetting(Movement::Up);
			}

			if (IsKeyPressed(keyDown))
			{
				characterSideMovementSetting(Movement::Down);
			}

			if (IsKeyPressed(keyJump))
			{
				characterJumpSetting();
			}
		}

		static void defeatCheck(Player player)
		{
			if (!player.lives > 0)
			{
				game::actualScene = Scenes::MainMenu;
				deinit();
			}
		}
	}

	namespace drawing
	{
		static void drawHUDs()
		{
			DrawText(TextFormat("Score: %i", player1.score), scorePos.x, scorePos.y, scoreSize, scoreColor);
			DrawText(TextFormat("Distance: %i", player1.distance), distancePos.x, distancePos.y, distanceSize, distanceColor);
			DrawText("Lives: ", livesPos.x, livesPos.y, livesSize, livesColor);

			int auxX = livesPos.x + MeasureText("Lives: ", livesSize);
			if (player1.lives > 0)
			{
				//DrawTexture(livesTexture, auxX, livesPos.y, livesColor);
				for (short i = 0; i < player1.lives; i++)
				{
					DrawTexture(livesTexture, auxX, livesPos.y, livesColor);
					auxX += livesTexture.width;
					
				}
			}
		}

		static void drawPlayer()
		{
			drawHUDs();
			DrawTextureRec(player1.character.texture, player1.character.animationFrame, Vector2{ player1.character.collider.x,player1.character.collider.y },player1.character.dye);
		}

		static void drawStage()
		{
			DrawTexture(level1.background.texture,level1.background.pos.x, level1.background.pos.y, level1.background.dye);
			DrawTexture(level1.background.texture, level1.background.auxPosX, level1.background.pos.y, level1.background.dye);

			DrawTexture(level1.background.frontBackground.texture, level1.background.frontBackground.pos.x, level1.background.frontBackground.pos.y, level1.background.frontBackground.dye);
			DrawTexture(level1.background.frontBackground.texture, level1.background.frontBackground.auxPosX, level1.background.frontBackground.pos.y, level1.background.frontBackground.dye);
			
			DrawTexture(level1.floor.texture, level1.floor.pos.x, level1.floor.pos.y, level1.floor.dye);
			DrawTexture(level1.floor.texture, level1.floor.auxPosX, level1.floor.pos.y, level1.floor.dye);
		}

		static void drawObstacles()
		{
			for (short i = 0; i < maxAmountObstacles; i++)
			{
				if (obstacles[i].active)
				{
					DrawTexture(obstaclesTextures[(int)obstacles[i].tipe], obstacles[i].collider.x, obstacles[i].collider.y, obstacles[i].dye);					
					//DrawRectangleRec(obstacles[i].collider, obstacles[i].dye);
				}
			}
		}
	}

	namespace deinitialize
	{
		static void deinitPlayer()
		{
			UnloadTexture(player1.character.texture);
			UnloadTexture(livesTexture);
		}

		static void deinitStage()
		{
			for (short i = 0; i < obstacleTipes; i++)
			{
				UnloadTexture(obstaclesTextures[i]);
			}
			UnloadTexture(level1.background.texture);
			UnloadTexture(level1.background.frontBackground.texture);
			UnloadTexture(level1.floor.texture);			
		}
	}

	static void init()
	{
		using namespace initialize;
		
		obstaclesTimer = clock();
		timeIntervalForObstacles = 2500;

		auxTargetYPos = 0;
		auxJumpDistance = window::screen.height / 5;
		
		initStage();
		initPlayer();
		initObstaclesTextures();

		for (short i = 0; i < maxAmountObstacles; i++)
		{
			initObstacle(obstacles[i]);
		}

		initHUDs();
	}

	void update()
	{
		using namespace updating;

		background::parallaxMovement(level1);
		runCharacterAnimation();

		imput();
		for (short i = 0; i < maxAmountObstacles; i++)
		{
			collisions::playerObstaclesColision(player1,obstacles[i]);
		}
		
		characterLaneSwaping();
		characterJumping();
		generateObstacles();
		movingObtacles();
		defeatCheck(player1);
		player1.distance++;
		player1.score++;
	}

	void draw()
	{
		using namespace drawing;
		drawStage();		
		drawObstacles();
		drawPlayer();
	}
	
	void deinit()
	{
		using namespace deinitialize;

		initializedMenu = false;
		deinitPlayer();
		deinitStage();
	}

	void play()
	{
		if (!initializedMenu)
		{
			init();
			initializedMenu = true;
		}

		update();
	}

}