#ifndef COLLISIONS_H
#define COLLISIONS_H
#include "Gameplay/Entities/Obstacles/obstacles.h"
#include "Gameplay/Entities/Player/player.h"

namespace collisions
{
	void playerObstaclesColision(Player &player,Obstacle &obstacle);
}

#endif // !COLLISIONS_H

