#include "collisions.h"
#include "Controls/controls.h"

namespace collisions
{
	void playerObstaclesColision(Player& player, Obstacle& obstacle)
	{
		if (obstacle.active)
		{
			if (obstacle.lane==player.character.actualLane)
			{
				if (CheckCollisionRecs(player.character.collider, obstacle.collider))
				{
					if (obstacle.tipe==ObstacleTipe::Breakable)
					{
						if (IsKeyDown(controls::keySpecial))
						{
							player.score += 100;
						}
						else
						{
							player.lives--;
						}
						obstacle.active = false;
					}
					else
					{
						obstacle.active = false;
						player.lives--;
					}
				}
			}			
		}		
	}
}