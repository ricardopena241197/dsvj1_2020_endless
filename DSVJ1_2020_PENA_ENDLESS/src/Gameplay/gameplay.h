#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

enum class Movement
{
	Up,
	Down
};

namespace gameplay
{	
	const int maxAmountObstacles = 5;
	void play();
	void draw();
	void deinit();
}

#endif // !GAMEPLAY_H

