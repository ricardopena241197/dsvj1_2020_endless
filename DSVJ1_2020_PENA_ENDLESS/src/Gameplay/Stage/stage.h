#ifndef STAGE_H
#define STAGE_H
#include "raylib.h"
#include "Window/window.h"

struct Object
{
	int speed;
	Vector2 pos;
	int auxPosX;
	Texture2D texture;
	Color dye;
};
struct Background
{
	int speed;
	Object frontBackground;	
	Vector2 pos;
	int auxPosX;
	Texture2D texture;
	Color dye;
};
struct Floor
{	
	Vector2 pos;
	int auxPosX;
	int speed;	
	Texture2D texture;
	Color dye;
};
const int amountOfLanes = 3;
struct Stage
{
	Background background;
	Floor floor;
	int lanePosY[amountOfLanes];
};
namespace background
{	
	void parallaxMovement(Stage &level);
}
#endif // !BACKGROUD_H

