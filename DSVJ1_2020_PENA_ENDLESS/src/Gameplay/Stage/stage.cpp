#include "stage.h"

namespace background
{
	static void moveBackground(Stage &level)
	{
		if (level.background.pos.x + level.background.texture.width >= 0)
		{
			level.background.pos.x -= level.background.speed * GetFrameTime();
		}
		else
		{
			level.background.pos.x = 0;
		}
		level.background.auxPosX = level.background.pos.x + level.background.texture.width;
	}

	static void moveFrontBackground(Stage &level)
	{
		if (level.background.frontBackground.pos.x + level.background.frontBackground.texture.width >= 0)
		{
			level.background.frontBackground.pos.x -= level.background.frontBackground.speed * GetFrameTime();
		}
		else
		{
			level.background.frontBackground.pos.x = 0;
		}
		level.background.frontBackground.auxPosX = level.background.frontBackground.pos.x + level.background.frontBackground.texture.width - 2;
	}

	static void moveFloor(Stage& level)
	{
		if (level.floor.pos.x + level.floor.texture.width >= 0)
		{
			level.floor.pos.x -= level.floor.speed * GetFrameTime();
		}
		else
		{
			level.floor.pos.x = 0;
		}
		level.floor.auxPosX = level.floor.pos.x + level.floor.texture.width - 1;
	}

	void parallaxMovement(Stage &level)
	{
		moveBackground(level);
		moveFrontBackground(level);
		moveFloor(level);
	}
}