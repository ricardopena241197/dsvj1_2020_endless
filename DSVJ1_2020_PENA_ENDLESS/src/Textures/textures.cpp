#include "textures.h"


namespace textures
{
	int	buttonBorderWidth;
	void initTitleTexture(Texture2D& texture, MenuOptions option)
	{
		texture.height = option.textSize;
		texture.width = option.size + option.size / 2;
	}

	void initOptionsTexture(Texture2D& texture, MenuOptions option)
	{
		buttonBorderWidth = window::screen.height / 25;
		texture.height = option.textSize + buttonBorderWidth / 2;
		texture.width = option.size + buttonBorderWidth;
	}
}