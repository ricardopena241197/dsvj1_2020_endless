#ifndef TEXTURES_H
#define TEXTURES_H

#include "Menus/Main Menu/main_menu.h"
#include "raylib.h"

namespace textures
{
	//File path for texture assets

	//Gameplay
		//Stage
	const char floorTexturePath[] = "res/assets/textures/gameplay/stage/floor.png";
	const char backgroundTexturePath[] = "res/assets/textures/gameplay/stage/background/background.png";
	const char frontBackgroundTexturePath[] = "res/assets/textures/gameplay/stage/background/front_background.png";
		//Character
	const char characterTexturePath[] = "res/assets/textures/gameplay/character/chick_animation.png";
	const char livesTexturePath[] = "res/assets/textures/gameplay/character/lives.png";
		//Obstacles
	const char jumpeableObsTexturePath[]= "res/assets/textures/gameplay/obstacles/tree.png";
	const char breakableObsTexturePath[] = "res/assets/textures/gameplay/obstacles/corn.png";
	const char skippeableObsTexturePath[] = "res/assets/textures/gameplay/obstacles/barrel.png";	

	//Menus
	const char optionsTexturePath[] = "res/assets/textures/menus/options_texture.png";

	extern int	buttonBorderWidth;
	void initTitleTexture(Texture2D& texture, MenuOptions option);
	void initOptionsTexture(Texture2D& texture, MenuOptions option);
}

#endif // !TEXTURES_H

