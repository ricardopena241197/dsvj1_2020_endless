#include "game.h"
#include "raylib.h"
#include "Global Variables/global_variables.h"
#include "Window/window.h"
#include "Gameplay/gameplay.h"
#include "Controls/controls.h"
#include "Menus/Main Menu/main_menu.h"
#include "Gameplay/Stage/stage.h"
#include "Textures/textures.h"
#include "Menus/Credits/credits.h"
#include "Menus/Settings/settings.h"
#include "Sounds/sounds.h"
#include "Menus/Instructions/instructions.h"

#include <iostream>
#include <time.h>

namespace game
{
	Scenes actualScene;

	Stage background;

	namespace initialize
	{
		static void initStage()
		{
			using namespace textures;

			//Init Background
			background.background.texture = LoadTexture(backgroundTexturePath);
			background.background.texture.height = window::screen.height / 2 * 1.25;
			background.background.texture.width = window::screen.width;
			background.background.dye = WHITE;

			background.background.pos.x = 0;
			background.background.pos.y = 0;
			background.background.auxPosX = background.background.pos.x + background.background.texture.width;

			background.background.speed = (window::screen.width / 30);

			//Init FrontBackground
			background.background.frontBackground.texture = LoadTexture(frontBackgroundTexturePath);
			background.background.frontBackground.texture.height = (window::screen.height / 2) * 1.25;
			background.background.frontBackground.texture.width = window::screen.width;
			background.background.frontBackground.dye = WHITE;

			background.background.frontBackground.pos.x = 0;
			background.background.frontBackground.pos.y = background.background.pos.y + background.background.texture.height / 4;
			background.background.frontBackground.auxPosX = background.background.frontBackground.pos.x + background.background.frontBackground.texture.width - 2;

			background.background.frontBackground.speed = window::screen.width / 20;

			//Init floor
			background.floor.texture = LoadTexture(floorTexturePath);
			background.floor.texture.height = (window::screen.height / 3);
			background.floor.texture.width = window::screen.width;
			background.floor.dye = WHITE;

			background.floor.pos.x = 0;
			background.floor.pos.y = (window::screen.height / 3) * 2;
			background.floor.auxPosX = background.floor.pos.x + background.floor.texture.width - 1;

			background.floor.speed = window::screen.width / 6;

			for (short i = 0; i < amountOfLanes; i++)
			{
				background.lanePosY[i] = (int)(background.floor.pos.y + (background.floor.texture.height / 4) * (i + 1));
			}
		}
	}

	namespace drawing
	{
		static void drawStage()
		{
			DrawTexture(background.background.texture, background.background.pos.x, background.background.pos.y, background.background.dye);
			DrawTexture(background.background.texture, background.background.auxPosX, background.background.pos.y, background.background.dye);

			DrawTexture(background.background.frontBackground.texture, background.background.frontBackground.pos.x, background.background.frontBackground.pos.y, background.background.frontBackground.dye);
			DrawTexture(background.background.frontBackground.texture, background.background.frontBackground.auxPosX, background.background.frontBackground.pos.y, background.background.frontBackground.dye);

			DrawTexture(background.floor.texture, background.floor.pos.x, background.floor.pos.y, background.floor.dye);
			DrawTexture(background.floor.texture, background.floor.auxPosX, background.floor.pos.y, background.floor.dye);
		}
	}

	static void init()
	{
		actualScene = Scenes::MainMenu;
		window::init();
		controls::init();
		srand(time(NULL));
		initialize::initStage();
		InitAudioDevice();
		sounds::initSounds();
	}

	static void update()
	{
		if (actualScene!=Scenes::Play)
		{
			background::parallaxMovement(background);
			PlayMusicStream(sounds::mainMenu);
			UpdateMusicStream(sounds::mainMenu);
		}
		else
		{
			PlayMusicStream(sounds::gamePlay);
			UpdateMusicStream(sounds::gamePlay);
		}

		switch (actualScene)
		{
		case Scenes::MainMenu:
			main_menu::menu();
			break;
		case Scenes::Play:
			gameplay::play();
			break;
		case Scenes::Settings:
			settings::menu();
			break;
		case Scenes::Credits:
			credits::menu();
			break;
		case Scenes::instructions:
			instructions::menu();
			break;
		default:
			break;
		}

		sounds::playEfects();
	}

	static void draw()
	{
		BeginDrawing();
		ClearBackground(RAYWHITE);

		if (actualScene != Scenes::Play)
		{
			drawing::drawStage();
		}
		switch (actualScene)
		{
		case Scenes::MainMenu:
			main_menu::draw();
			break;
		case Scenes::Play:
			gameplay::draw();
			break;
		case Scenes::Settings:
			settings::draw();
			break;
		case Scenes::Credits:
			credits::draw();
			break;
		case Scenes::instructions:
			instructions::draw();
			break;
		default:
			break;
		}
		EndDrawing();
	}

	static void deinit()
	{
		CloseAudioDevice();
		sounds::deinitSounds();
	}

	void game()
	{
		init();
		global_variables::auxExit = false;
		while (!WindowShouldClose() && !global_variables::auxExit)
		{
			update();
			draw();
		}
		deinit();
	}

}