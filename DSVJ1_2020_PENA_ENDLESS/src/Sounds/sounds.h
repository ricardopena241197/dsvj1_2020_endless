#ifndef SOUNDS_H
#define SOUNDS_H
#include "raylib.h"

namespace sounds
{
	extern Music mainMenu;	
	extern Music gamePlay;
	extern bool musicActive;
	extern bool soundsActive;

	extern Sound movingMenu;
	extern bool playMovingMenu;

	extern Sound acceptMenu;
	extern bool playAccepMenu;

	void initSounds();
	void muteUnmuteMusic();
	void muteUnmuteEfects();
	void deinitSounds();
	void playEfects();	

}

#endif // !SOUNDS_H

