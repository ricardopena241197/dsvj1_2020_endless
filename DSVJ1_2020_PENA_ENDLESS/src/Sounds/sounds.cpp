#include "sounds.h"

namespace sounds
{
	Music mainMenu = LoadMusicStream("res/assets/music/gameplay/menus_music.mp3");
	Music gamePlay = LoadMusicStream("res/assets/music/gameplay/gameplay_music.wav");
	bool musicActive = true;

	Sound movingMenu = LoadSound("res/assets/music/gameplay/move.wav");
	bool playMovingMenu = false;

	Sound acceptMenu = LoadSound("res/assets/music/gameplay/move.wav");
	bool playAccepMenu = false;

	bool soundsActive = true;

	void muteUnmuteMusic()
	{
		musicActive = !musicActive;
		if (musicActive)
		{
			SetMusicVolume(mainMenu, 1);
			SetMusicVolume(gamePlay, 1);
		}
		else
		{
			SetMusicVolume(mainMenu, 0);
			SetMusicVolume(gamePlay, 0);
		}
	}

	void muteUnmuteEfects()
	{
		soundsActive = !soundsActive;
		if (soundsActive)
		{
			SetSoundVolume(movingMenu, 1);
			SetSoundVolume(acceptMenu, 1);
		}
		else
		{
			SetSoundVolume(movingMenu, 0);
			SetSoundVolume(acceptMenu, 0);
		}
	}

	void initSounds()
	{
		mainMenu = LoadMusicStream("res/assets/music/menus_music.mp3");
		gamePlay = LoadMusicStream("res/assets/music/gameplay_music.mp3");
		musicActive = false;

		movingMenu = LoadSound("res/assets/music/move.wav");
		playMovingMenu = false;

		acceptMenu = LoadSound("res/assets/music/move.wav");
		playAccepMenu = false;

		soundsActive = false;
		muteUnmuteEfects();
		muteUnmuteMusic();
	}	

	void deinitSounds()
	{
		UnloadMusicStream(mainMenu);
		UnloadMusicStream(gamePlay);
		UnloadSound(movingMenu);
		UnloadSound(acceptMenu);
	}

	void playEfects()
	{
		if (playMovingMenu)
		{
			playMovingMenu = false;
			PlaySound(movingMenu);
		}

		if (playAccepMenu)
		{
			playAccepMenu = false;
			PlaySound(acceptMenu);
		}
	}
}