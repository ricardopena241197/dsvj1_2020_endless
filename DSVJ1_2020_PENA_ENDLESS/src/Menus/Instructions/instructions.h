#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H
#include "raylib.h"

namespace instructions
{
	enum class menuOptions { Exit };

	const int optionsAmount = 1;
	const int maximumAmountOfCharacters = 100;

	const Color defaultTitleColor = YELLOW;
	const Color defaultOptionsColor = YELLOW;
	const Color alternativeOptionsColor = RED;

	void draw();
	void deinit();
	void menu();
}

#endif // !INSTRUCTIONS_H

