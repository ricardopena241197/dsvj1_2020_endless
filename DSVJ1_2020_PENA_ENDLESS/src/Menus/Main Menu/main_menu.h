#ifndef MAIN_MENU
#define MAIN_MENU
#include "raylib.h"
#include "Window/window.h"

#include <iostream>
#include <string.h>

using namespace std;

struct MenuOptions
{
	Vector2 pos;
	int textSize;
	int size;
	Color color;
	string text;
	Texture2D texture;
	Color dye;
};

namespace main_menu
{
	enum class menuOptions { Play, Settings, Credits, Instructions, Exit };
	
	const int optionsAmount = 5;
	const int maximumAmountOfCharacters = 20;

	const Color defaultTitleColor = YELLOW;
	const Color defaultOptionsColor = YELLOW;
	const Color alternativeOptionsColor = RED;
			
	void draw();
	void deinit();
	void menu();
}

#endif // !MAIN_MENU

