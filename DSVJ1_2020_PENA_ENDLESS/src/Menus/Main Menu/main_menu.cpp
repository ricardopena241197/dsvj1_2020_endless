#include "main_menu.h"
#include "Window/window.h"
#include "Textures/textures.h"
#include "Text/text.h"
#include "Controls/controls.h"
#include "Game/game.h"
#include "Global Variables/global_variables.h"
#include "Sounds/sounds.h"


namespace main_menu
{	
	int spaceBetweenOptions;

	bool initializedMenu = false;

	MenuOptions title;
	MenuOptions options[optionsAmount];
	Color colorOption = BLACK;
	
	int iAuxCursor = 0;

	namespace initialize
	{
		void initializeTitle()
		{
			title.pos.y = window::screen.height/100;
			title.textSize = window::screen.width/15;
			title.color = defaultTitleColor;
			title.texture = LoadTexture(textures::optionsTexturePath);
			title.dye = WHITE;
			title.text = "ChickRunner";
			title.size = text::measureStringText(title.text, title.textSize);
			title.pos.x = window::screen.width/2 - title.size / 2;
			textures::initTitleTexture(title.texture, title);			
		}
		
		void initializeOptions()
		{
			spaceBetweenOptions=window::screen.height/(optionsAmount+1);

			options[0].pos.y = window::screen.height / 4;
			options[0].textSize = window::screen.width / 20;
			options[0].color = defaultOptionsColor;
			options[0].texture = LoadTexture(textures::optionsTexturePath);
			options[0].dye = WHITE;
			options[0].text = "Play";
			options[0].size = text::measureStringText(options[0].text, options[0].textSize);
			options[0].pos.x = window::screen.width/2 - options[0].size / 2;
			textures::initOptionsTexture(options[0].texture, options[0]);

			for (short i = 1; i < optionsAmount; i++)
			{
				options[i].pos.y = options[i - 1].pos.y + spaceBetweenOptions;
				options[i].textSize = window::screen.width / 20;
				options[i].color = defaultOptionsColor;
				options[i].texture = options[i - 1].texture;
				options[i].dye = WHITE;

				switch (i)
				{
				case 1:
					options[i].text = "Settings";
					break;
				case 2:
					options[i].text = "Credits";
					break;
				case 3:
					options[i].text = "Instructions";
					break;
				case 4:
					options[i].text = "Exit";
					break;

				default:
					break;
				}
				options[i].size = text::measureStringText(options[i].text, options[i].textSize);

				options[i].pos.x = window::screen.width / 2 - options[i].size / 2;
				textures::initOptionsTexture(options[i].texture, options[i]);
			}
		}
		
	}

	namespace drawing
	{
		void drawTitle()
		{
			char auxTexto[maximumAmountOfCharacters];
			strcpy_s(auxTexto, title.text.c_str());
			DrawTexture(title.texture, title.pos.x - title.size / 4, title.pos.y, title.dye);
			DrawText(auxTexto, title.pos.x, title.pos.y, title.textSize, title.color);

		}

		void drawOptions()
		{
			using namespace textures;
			char auxTexto[maximumAmountOfCharacters];

			for (short i = 0; i < optionsAmount; i++)
			{
				if (iAuxCursor == i)
				{
					options[i].color = alternativeOptionsColor;
				}
				else
				{
					options[i].color = defaultOptionsColor;
				}

				strcpy_s(auxTexto, options[i].text.c_str());

				DrawTexture(options[i].texture, options[i].pos.x - buttonBorderWidth / 2, options[i].pos.y - buttonBorderWidth / 4, options[i].dye);
				DrawText(auxTexto, options[i].pos.x, options[i].pos.y, options[i].textSize, options[i].color);
			}
		}
	}

	static void init()
	{
		using namespace initialize;

		initializeTitle();
		initializeOptions();		

		iAuxCursor = 0;
	}

	static void update()
	{
		using namespace controls;


		if (IsKeyPressed(keyUp)) { iAuxCursor = (iAuxCursor == 0) ? optionsAmount - 1 : iAuxCursor - 1; sounds::playMovingMenu = true; }
		if (IsKeyPressed(keyDown)) { iAuxCursor = (iAuxCursor == optionsAmount - 1) ? 0 : iAuxCursor + 1; sounds::playMovingMenu = true;}
		if (IsKeyPressed(keyEnter))
		{
			
			sounds::playAccepMenu = true;
			switch ((menuOptions)iAuxCursor)
			{
			case menuOptions::Play:
				game::actualScene = Scenes::Play;				
				break;
			case menuOptions::Settings:
				game::actualScene = Scenes::Settings;
				break;
			case menuOptions::Credits:
				game::actualScene = Scenes::Credits;
				break;
			case menuOptions::Instructions:
				game::actualScene = Scenes::instructions;
				break;
			case menuOptions::Exit:
				global_variables::auxExit = true;
				deinit();
				break;
			default:
				break;
			}

		}
	}

	void draw()
	{
		using namespace drawing;			

		drawTitle();

		drawOptions();

	}

	void deinit()
	{		
		initializedMenu = false;
		UnloadTexture(title.texture);
		UnloadTexture(options[0].texture);		
	}

	void menu()
	{
		if (!initializedMenu)
		{
			init();
			initializedMenu = true;
		}
		update();
	}
	
}