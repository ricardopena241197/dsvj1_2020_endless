#ifndef SETTINGS_H
#define SETTINGS_H

#include "raylib.h"

#include <iostream>
#include <string.h>

namespace settings
{
	enum class menuOptions { Music, Sounds, Back };

	const int optionsAmount = 3;
	const int maximumAmountOfCharacters = 100;

	const Color defaultTitleColor = YELLOW;
	const Color defaultOptionsColor = YELLOW;
	const Color alternativeOptionsColor = RED;

	void draw();
	void deinit();
	void menu();
}

#endif // !SETTINGS_H
