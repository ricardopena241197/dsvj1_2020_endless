#include "controls.h"
#include "raylib.h"

namespace controls
{
	int keyUp;
	int keyDown;
	int keyJump;
	int keySpecial;
	int keyPause;
	int keyEnter;
	void init()
	{
		//Default Controls
		keyUp=KEY_UP;
		keyDown = KEY_DOWN;
		keyJump = KEY_SPACE;
		keySpecial = KEY_V;
		keyPause = KEY_P;
		keyEnter = KEY_ENTER;
	}
}
